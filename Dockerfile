FROM ros:melodic

RUN apt-get update -y && apt-get upgrade -y

RUN apt-get install -y git curl wget python3-pip

RUN sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list' && \
    wget https://packages.osrfoundation.org/gazebo.key -O - | apt-key add -                                                                       && \
    apt-get update -y                                                                                                                             && \
    apt-get install -y gazebo9

RUN git clone https://github.com/PX4/Firmware

WORKDIR Firmware

# We use this version since in Ubuntu 18.04 the package kconfig-frontend cannot
# be found. In more recent versions of the Firmware the `Tools/setup/ubuntu.sh`
# script uses `set -e` which will terminate the script if any individual command
# fails. However, the rest of the script will work despite the failure with
# kconfig-frontend.
#
# TODO: Fix this to use a more recent version of the Firmware.
RUN git checkout dddd2c3297bd57a92
RUN apt-get install -y libjpeg-dev

RUN bash Tools/setup/ubuntu.sh

RUN DONT_RUN=1 make px4_sitl_default gazebo

RUN apt-get install -y ros-melodic-gazebo-ros-pkgs ros-melodic-gazebo-ros-control

CMD bash -c 'source /opt/ros/$ROS_DISTRO/setup.bash                              && \
             source Tools/setup_gazebo.bash $(pwd) $(pwd)/build/px4_sitl_default && \
             export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$(pwd)                    && \
             export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$(pwd)/Tools/sitl_gazebo  && \
             export PX4_HOME_LAT=-19.468335                                      && \
             export PX4_HOME_LON=-49.143521                                      && \
             export PX4_HOME_ALT=847.142652                                      && \
             roslaunch px4 posix_sitl.launch gui:=false'
